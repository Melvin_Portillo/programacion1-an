%funcion de ln(1 + x) = x - (x^2)/2 + (x^3)/3 ...

x= input('Ingrese el valor de x (-1<x<1) ');
while(x<=-1 || x>=1)
    x= input('Ingrese el valor de x (-1<x<1) ');
end

cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 15): ');

Es = 0.5*(10^(2-cifras));
ite = 1;
resultado = 0;
resulAnt = 0;
Ea = 100000000;

while (Ea > Es)
    resulAnt=resultado;
    resultado=resultado+(((-1)^(ite-1))/ite*(x^ite));
    Ea=abs(resultado-resulAnt)/abs(resultado)*100;
    ite=ite+1;
    
end
fprintf('\nel resultado de la funcion ln(1 + %g) es: %f\n', x, resultado);

fprintf('el error aproximado es: %f\n', Ea);

valor=log(1+x);
fprintf('el valor real es: %f\n', valor);

Er=abs(valor-resultado);
fprintf('Er = %d\n', Er);

Err=Er/abs(valor);
fprintf('Err = %d\n', Err);

Ep=Err*100;
fprintf('Ep = %d\n', Ep);
