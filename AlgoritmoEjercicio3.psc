Funcion fact <- facto ( x )
	Si x=1 | x=0 Entonces
		fact=1
	SiNo
		c=1
		Para v<-1 Hasta x Con Paso 1 Hacer
			c=c*v
		Fin Para
		fact=c
	Fin Si
Fin Funcion

Algoritmo Ejercicio_3
	Escribir 'ingrese un valor para x: '
	Leer x
	Escribir 'ingrese la cantidad de cifras significativas(entre 1 y 12): '
	Leer cifras
	
	Ess= 0.5*(10^(2-cifras));
	ite=0;
	result=0;
	resultAn=0;
	Ea=100000000;
	
	Mientras Ea>Ess Hacer
		Si result=0 Entonces
			result= result + (((x)^ite)/facto(ite));
			ite=ite+1;
		SiNo
			resultAn=result;
			result= result + (((x)^ite)/facto(ite));
			ite=ite+1;
			Ea=abs((result-resultAn)/result)*100;
		Fin Si
	Fin Mientras
	Escribir 'el resultado de la funcion  e^x es: ',result
	Escribir 'el error aproximado es: ',Ea
	val=2.7182818284590452353^(x);
	Escribir 'valor real: ',val
	Er=val-result;
	Err=Er/val;
	Ep=Err*100;
	Escribir 'el error cometido es: ',Ep
	
FinAlgoritmo
