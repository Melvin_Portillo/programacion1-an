%funcion de sen x = x - (x^3)/3! +(x^5)/5!....

x = input('ingrese un valor para x: ');

cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 15): ');

Es = 0.5*(10^(2-cifras));
ite = 0;
resultado = 0;
resulAnt = 0;
Ea = 100000000;

while (Ea > Es)
  if (resultado == 0)
    resultado = resultado + (((-1)^ite)/factorial((2*ite + 1)))*(x^(2*ite +1));
    ite = ite+1;
  else 
    resulAnt = resultado;
    resultado = resultado + (((-1)^ite)/factorial((2*ite + 1)))*(x^(2*ite +1));
    ite = ite+1;
    Ea = abs((resultado - resulAnt)/resultado)*100;
  end
end

fprintf('\nel resultado de la funcion sen(%g) es: %f\n', x, resultado );

fprintf('el error aproximado es: %f\n', Ea);

valor=sin(x);
fprintf('el valor real es: %f\n', valor);

Er=abs(valor-resultado);
fprintf('Er = %d\n', Er);

Err=Er/abs(valor);
fprintf('Err = %d\n', Err);

Ep=Err*100;
fprintf('Ep = %d %\n', Ep);  


  